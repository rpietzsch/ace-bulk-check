# ace Bulk Check

Simple command collection to bulk check a collection of epub files with ace[^1].
Result summary and individual errors are extracted into TSV files for further processing (e.g. in Excel).

## Prerequisites

- task(file): <https://taskfile.dev/installation/>
- ace: <https://github.com/daisy/ace/releases>
- jq: <https://jqlang.github.io/jq/>

## Usage

- copy the EPUB files to test into the `./epubs/` folder (subfolders will work, too)
- run `task check`
- examine the result files:
  - `./epub-summary.tsv` overall ace summary (pass / fail)
  - `./epub-errors.tsv` detailed error description for those that did not `pass` the ace check

[^1]: Ace by DAISY, an Accessibility Checker for EPUB: <https://github.com/daisy/ace>
